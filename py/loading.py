import time

class Loading:

    def __init__( self , max_iter , prefix='Progress:', suffix='Complete' , length=100 , decimals = 1, fill = '█', print_end = "\r" ):
        """
        Call in a loop to create terminal progress bar
        @params:
            max_iter    - Required  : self.max_iter iterations (Int)
            prefix      - Optional  : prefix string (Str)
            suffix      - Optional  : suffix string (Str)
            length      - Optional  : character length of bar (Int)
            decimals    - Optional  : positive number of decimals in percent complete (Int)
            fill        - Optional  : bar fill character (Str)
            print_end   - Optional  : end character (e.g. "\r", "\r\n") (Str)
        """        
        self.max_iter  = max_iter
        self.prefix    = prefix
        self.suffix    = suffix
        self.length    = length
        self.decimals  = decimals
        self.fill      = fill
        self.print_end = print_end
        

    def print( self , iteration ):
        percent = ("{0:." + str(self.decimals) + "f}").format(100 * (iteration / float(self.max_iter)))
        filledLength = int(self.length * iteration // self.max_iter)
        bar = self.fill * filledLength + '-' * (self.length - filledLength)
        print(f'\r{self.prefix} |{bar}| {percent}% {self.suffix}', end = self.print_end)
        # Print New Line on Complete
        if iteration == self.max_iter: 
            print()


if __name__ == '__main__':
    loading = Loading( max_iter=80, prefix='Progress:', suffix='Complete', length=50 )
    for i in range(0,81):
        time.sleep(0.01)
        loading.print( i )