from docx import Document
import re
import os
import cv2
import pytesseract

TESSERACT_BIN = r'C:\Users\jmauricio\AppData\Local\Programs\Tesseract-OCR\tesseract.exe'

class OCR:

    def __init__( self ):
        pytesseract.pytesseract.tesseract_cmd = TESSERACT_BIN


    def img_to_txt( self , item_pics ):
        #img = cv2.imread( item_pics[0] , 0 )
        img = cv2.imread( "test.jpg" , 0 )
        custom_config = r'--oem 3 --psm 6'
        return pytesseract.image_to_string(img, config=custom_config)


    def docx_new( self , docx_filename ):
        self.docx_filename = docx_filename
        self.file_docx = Document()


    def docx_add_page( self , i_page , txt ):
        if i_page > 1: self.file_docx.add_page_break( )
        self.file_docx.add_heading("Pàgina {:d}".format(i_page), 0)
        self.file_docx.add_paragraph(txt)


    def docx_save( self ):
        self.file_docx.save( self.docx_filename )


    def img_to_docx( self , item_pics , docx_file ):
        self.docx_new( docx_file )
        for i_page,pic_path in enumerate(item_pics):
            txt = self.img_to_txt( pic_path )
            self.docx_add_page( i_page , txt )
        self.docx_save( )


if __name__ == '__main__':
    ocr = OCR()
    ocr.docx_new( "test.docx" )
    ocr.docx_add_page( 1 , "Test text 1" )
    ocr.docx_add_page( 2 , "Test text 2" )
    ocr.docx_add_page( 3 , "Test text 3" )
    ocr.docx_save( )
