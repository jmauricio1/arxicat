from PIL import Image  # install by > python3 -m pip install --upgrade Pillow  # ref. https://pillow.readthedocs.io/en/latest/installation.html#basic-installation
from scraper import Scraper
from ocr import OCR
import os
import shutil

WORK_DIR  = "C:\\Users\\jmauricio\\workspace\\ACRE\\ACRE340-36 - Artur Bladé Desumvila"
DATA_JSON = "data.json"

class Arxicat:

    def __init__( self , work_dir , keep_jpg=True , overwrite=False , pdf_resolution=100.0 ):
        self.keep_jpg       = keep_jpg    #No esborra les imatges en acabar
        self.overwrite      = overwrite   #Sobreescriu una entrada preexistent 
        self.pdf_resolution = pdf_resolution  #Qualitat d'imatge del PDF 0%-100%
        self.pdf_filename   = ""
        self.scraper        = Scraper( work_dir , overwrite=overwrite )
        os.environ["PYDEVD_WARN_EVALUATION_TIMEOUT"] = "10"


    def __pdf_skip_create( self , i_item ):
        self.pdf_filename = self.scraper.get_pdf_filename(i_item)
        return os.path.exists( self.pdf_filename ) and not self.overwrite

    def __pdf_save( self , pics , pdf_path ):
        print("INFO : Gravant PDF: {:s}...".format(pdf_path))
        images = [ Image.open( f ) for f in pics ]
        images[0].save( pdf_path , "PDF" , resolution=self.pdf_resolution , save_all=True , append_images=images[1:] )        


    def __remove_img( self , img_dir ):
        if os.path.exists( img_dir ):
            try:
                shutil.rmtree( img_dir )
            except Exception as e:
                print("ERROR: no s'ha pogut eliminar el directori "+img_dir)


    def get_n_items( self ):
        return self.scraper.load_json( DATA_JSON )


    def download_and_save_pdf( self , i_item ):
        #Crea els directoris i subdirectoris per l'arxiu:
        self.scraper.create_item( i_item )        
        item_pics = self.scraper.process_item( i_item )
        if item_pics==[] : 
            return []
        if not self.__pdf_skip_create( i_item ): 
            self.__pdf_save( item_pics , self.pdf_filename )
        if not self.keep_jpg:
            self.__remove_img( self.scraper.get_img_dir() )
        return item_pics




if __name__ == '__main__':
    arxicat = Arxicat( WORK_DIR , keep_jpg=True , overwrite=True , pdf_resolution=100.0 )
    ocr = OCR( )

    for i_item in range( 0 , arxicat.get_n_items() ):
        item_pics = arxicat.download_and_save_pdf( i_item )
        if item_pics==[] : continue
        ocr.img_to_docx( item_pics , "test.docx" )
        exit
