import os
import json
import time
import requests
#Loading bar:
from loading import Loading
#Selenium Web Driver stuff
from selenium import webdriver 
from selenium.webdriver.chrome.service import Service as ChromeService 
from webdriver_manager.chrome import ChromeDriverManager 
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait 

DEBUG = True
IMG_ELEMENT = "ngx-gallery-image"
WEB_TIMEOUT_SECS = 30

class Scraper:

    def __init__( self , work_dir, overwrite=True ):
        self.data = []
        self.work_dir     = work_dir    #Directori local amb la biblioteca d'arxius
        self.overwrite    = overwrite   #Sobreescriu una entrada preexistent
        #Configuració del WebDriver:
        self.wd_options = webdriver.ChromeOptions() 
        if not DEBUG : 
            self.wd_options.add_argument('--headless')    #No mostra la pàgina web Chrome
            self.wd_options.add_argument('--log-level=3')  
        #Crea el directori Biblioteca en cas que no existeixi:
        self.__mkdir_safe( self.work_dir )


    def __mkdir_safe( self , dir ):
        if( os.path.isdir(dir) ): return
        try:
            os.mkdir( dir )
        except Exception as e:
            print("FATAL: No s'ha pogut crear el directori "+dir)
            print("FATAL: Motiu: ",str(e))
            exit( 0 )


    def create_item( self , i_item ):
        self.item_id      = self.data[i_item]["element"]["codi_referencia"]
        self.item_dir     = os.path.join( self.work_dir , self.item_id )
        self.item_dir_img = os.path.join( self.item_dir, "img" )
         
        if os.path.isdir( self.item_dir ):
            print("AVÍS : El directori {:s} ja existeix. {:s} es sobreescriurà...".format(self.item_dir,"SÍ" if self.overwrite else "NO") )

        self.__mkdir_safe( self.item_dir     )
        self.__mkdir_safe( self.item_dir_img )


    def __find_pic_pattern_by_url( self , url ):
        try:
            with webdriver.Chrome(service=ChromeService(ChromeDriverManager().install()), options=self.wd_options) as driver: 
                driver.get(url)
                #time.sleep(5) 
                try:
                    gallery_class = WebDriverWait(driver, WEB_TIMEOUT_SECS).until(lambda x: x.find_element(By.CLASS_NAME,IMG_ELEMENT)) 
                except Exception as e:                 
                    print("ERROR: No s'ha pogut trobar la galeria d'imatges dins la pàgina web (class=\"{:s}\")".format(IMG_ELEMENT))
                    if DEBUG : print("ERROR: Motiu: ",str(e))
                    return ""
                try:
                    gallery_class_style = gallery_class.get_attribute("style")
                    #Ex: gallery_class_style = 'background-image: url("https://static.arxiusenlinia.cultura.gencat.cat/active/GIAC/TEXTUAL/340/11/1/1364/doc_ACRE_1349032_1364,0001.jpg");'
                    #    return              = https://static.arxiusenlinia.cultura.gencat.cat/active/GIAC/TEXTUAL/340/11/1/1364/doc_ACRE_1349032_1364,{04d}.jpg
                    return gallery_class_style.split("\"")[1].replace(",0001.jpg",",{:04d}.jpg")
                except Exception as e:
                    print("ERROR: No s'ha trobat l'atribut 'style' dins la galeria d'imatges")
                    if DEBUG : print("ERROR: Motiu: ",str(e))
                    return ""                 
        except Exception as e:
            print("ERROR:   - No s'ha pogut carregar la URL!")
            if DEBUG : print("ERROR: Motiu: ",str(e))
            return ""    


    def load_json(self, filename):
        with open(filename) as f:
            self.data = json.load(f)
        return len(self.data)


    def save_item_json( self , data ):
        json_file = os.path.join( self.item_dir , data["element"]["codi_referencia"]+".json" )
        print("INFO : Gravant la base de dades {:s}...".format(json_file))
        with open( json_file , 'w') as f:
            json.dump( data , f )


    def get_pdf_filename( self , i_item ):
        return os.path.join( self.item_dir , self.data[i_item]["element"]["codi_referencia"]+".pdf" )        


    def get_img_dir( self ):
        return self.item_dir_img


    def get_pics_url(self, i_item):
        url = self.data[i_item]["element"]["enllac_objecte_digital"]
        print("INFO : Analitzant URL: " + url )
        #Obté l'objecte Web Driver i n'extreu el patró per generar les URLs de les imatges:
        pic_pattern = self.__find_pic_pattern_by_url( url )
        if pic_pattern == "": return []
        #Si tot ha anat bé, crea la llista d'imatges a descarregar a partir del patró:
        pics_url = []
        for i_pic in range(1,self.data[i_item]["element"]["total_objectes_digitals"]+1):
            pics_url.append( pic_pattern.format(i_pic) )
        print("INFO : S'ha obtingut la URL de les imatges :-)" )    
        return pics_url
    

    def download_pics( self , pics_url ):
        pfix = "Descarregant {:s}:".format(self.item_id)
        loading = Loading( max_iter=len(pics_url), prefix=pfix, suffix='Completat', length=80 )
        pic_paths   = []
        pics_url_ok = []
        for i_pic, pic_url in enumerate(pics_url):
            pic_path = os.path.join( self.item_dir_img , os.path.basename(pic_url) )            
            if( not os.path.exists(pic_path) or self.overwrite ): 
                try:
                    open(pic_path, 'wb').write( requests.get(pic_url, allow_redirects=True).content )
                except Exception as e:
                    print("ERROR: No s'ha pogut descarregar el fitxer: "+pic_url)
                    if DEBUG : print("ERROR: Motiu: ",str(e))
                    pics_url_ok.append( False )
                    continue
            pic_paths.append( pic_path ) 
            pics_url_ok.append( True )
            loading.print( i_pic+1 )
        return [ pic_paths , pics_url_ok ]

    def process_item( self , i_item ):
        pics_url  = self.get_pics_url( i_item )
        if len(pics_url) == 0: return []
        [ pic_paths , pics_url_ok ] = self.download_pics( pics_url )
        self.data[i_item]["url_pics"]    = pics_url
        self.data[i_item]["url_pics_ok"] = pics_url_ok
        self.save_item_json( self.data[i_item] )
        return pic_paths



if __name__ == '__main__':
    wdir = "C:\\Users\\jmauricio\\workspace\\ACREv2"
    scraper = Scraper( wdir , overwrite=False )
    #Carrega la base de dades amb els volumns a descarregar:
    n_items = scraper.load_json( "data.json" )
    #Analitza la URL principal de cada volum per saber on són les imatges i les descarrega
    for i_item in range(0,n_items):
        item_pics = scraper.process_item( i_item )        